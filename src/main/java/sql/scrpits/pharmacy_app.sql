/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Other/SQLTemplate.sql to edit this template
 */
/**
 * Author:  student
 * Created: 18-Jun-2022
 */
DROP DATABASE IF EXISTS PHARMACY_APP;
CREATE DATABASE IF NOT EXISTS PHARMACY_APP;
USE PHARMACY_APP;

CREATE TABLE IF NOT EXISTS DRUG_INFORMATION(ID INT PRIMARY KEY AUTO_INCREMENT,DRUG_CODE VARCHAR(20) NOT NULL,DRUG_NAME VARCHAR(20) NOT NULL,PRICE DECIMAL(8,2) NOT NULL,EXP_DATE DATE NOT NULL,DATE DATE NOT NULL);
DESC DRUG_INFORMATION;

CREATE TABLE IF NOT EXISTS PAYMENT_ITEMS(ID INT PRIMARY KEY AUTO_INCREMENT,DRUG_ID VARCHAR(20) NOT NULL,QUANTITY INT NOT NULL,UNIT_PRICE DECIMAL(8,2) NOT NULL,SUBQUANTITY DECIMAL(8,2) NOT NULL,PAYMENT_ID INT,FOREIGN KEY(PAYMENT_ID) REFERENCES DRUG_INFORMATION(ID));
DESC PAYMENT_ITEMS;