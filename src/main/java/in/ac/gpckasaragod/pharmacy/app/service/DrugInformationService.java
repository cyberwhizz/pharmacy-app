/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.pharmacy.app.service;

import in.ac.gpckasaragod.pharmacy.app.ui.data.DrugInformationDetails;
import java.util.Date;
import java.util.List;

/**
 *
 * @author student
 */
public interface DrugInformationService {

    /**
     *
     * @param drugCode
     * @param drugName
     * @param price
     * @param expDate
     * @param date
     * @return
     */
    public String saveDrugInformationDetailForm(String drugCode, String drugName, String price, Date expDate, Date date);
     public DrugInformationDetails readDrugInformationDetailsForm(Integer Id);
     public List<DrugInformationDetails> getAllDrugInformations();
     public String updateDrugInformationDetail(String drugCode, String drugName, String price, Date expDate, Date date);
     public String deleteDrugInformationDetail(Integer id);

  

   



}
