/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.pharmacy.app.impl;

import in.ac.gpckasaragod.pharmacy.app.service.PaymentItemsService;
import in.ac.gpckasaragod.pharmacy.app.ui.data.PaymentItemsDetails;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class PaymentServiceImpl extends ConnectionServiceImpl implements PaymentItemsService {
     public String saveDrugInformationDetailForm(String drugId, String paymentId, String quantity, String unitPrice, String subtotal) {
         try{
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "INSERT INTO PAYMENT_ITEMS (DRUG_ID,PAYMENT_ID,QUANTITY,UNIT_PRICE,SUBTOTAL) VALUES " +"('"+drugId+ "','" +paymentId+"','"+quantity+"','"+unitPrice+"','"+subtotal+"')" ;
            System.out.println("Query:"+query);
            int status = statement.executeUpdate(query);
            if(status!=1){
                return "Save failed";
                }
            else{
                 return "Saved successfully";
            }
        } catch (SQLException ex) {
            Logger.getLogger(DrugInformationServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            return "Save Failed";
    }

    

    

 }
  
    public List<PaymentItemsDetails> getAllPaymentItems() {
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody//
      
        List<PaymentItemsDetails> payments = new ArrayList<>();
        try{
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "SELECT * FROM PAYMENT_ITEMS";
            ResultSet resultSet = statement.executeQuery(query);
            
            while(resultSet.next()){
                int id = resultSet.getInt("ID");
                String drugId = resultSet.getString("DRUG_ID");
                String paymentId = resultSet.getString("PAYMENT_ID");
                String quantity = resultSet.getString("QUANTITY");
                String unitPrice = resultSet.getString("UNIT_PRICE");
                String subtotal = resultSet.getString("SUBTOTAL");
                PaymentItemsDetails PaymentItemsDetails= new PaymentItemsDetails(id, drugId, paymentId, quantity, unitPrice, subtotal);
                payments.add(PaymentItemsDetails);
            }
        } catch (SQLException ex) {
         Logger.getLogger(DrugInformationServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
     }
     return payments;
    }
    

     

    public PaymentItemsDetails readPaymentItemsDetailsForm(Integer Id) {
          PaymentItemsDetails   paymentItemsDetails = null;
        try{
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "SELECT * FROM PAYMENT_ITEMS WHERE ID="+Id;
            ResultSet resultSet = statement.executeQuery(query);
            while(resultSet.next()){
                int id = resultSet.getInt("ID");
                String drugId = resultSet.getString("DRUG_ID");
                String paymentId = resultSet.getString("PAYMENT_ID");
                String quantity = resultSet.getString("QUANTITY");
                String unitPrice = resultSet.getString("UNIT_PRICE");
                String subtotal = resultSet.getString("SUBTOTAL");
                paymentItemsDetails = new PaymentItemsDetails(id, drugId, paymentId, quantity, unitPrice, subtotal);
            }
            
            
        } catch (SQLException ex) {
         Logger.getLogger(DrugInformationServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
     }
       return paymentItemsDetails;
    }

    public String updatePaymentItemsDetail(String drugId, String paymentId, String quantity, String unitPrice, String subtotal) {
       try{
        Connection connection = getConnection();
        Statement statement = connection.createStatement();
        String query = "UPDATE PAYMENT_ITEMS SET DRUG_ID='"+drugId+"',PAYMENT_ID='"+paymentId+"',QUANTITY='"+quantity+"',UNIT_PRICE='"+unitPrice+"',SUBTOTAL='"+subtotal+"";
        System.out.print(query);
        int update = statement.executeUpdate(query);
        if(update !=1)
            return "Update failed";
        else
            return "Updated successfully";
       
    }   catch (SQLException ex) {
            Logger.getLogger(DrugInformationServiceImpl.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        return null;
    }

    public String deletePaymentItemsDetail(Integer id) {
        try{
            Connection connection =getConnection();
            String query = "DELETE FROM PAYMENT_ITEMS WHERE ID=?";
            PreparedStatement statement = connection.prepareCall(query);
            statement.setInt(1, id);
            int delete = statement.executeUpdate();
            if (delete !=1)
                return "Delete Failed";
            else
                return "Deleted Successfully";
            
            
        } catch (Exception ex) {
            ex.printStackTrace();
            return "Deleted Failed";
    }
    }

     @Override
    public Connection getConnection() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public String deletePaymentItemsDetails(Integer id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
}

