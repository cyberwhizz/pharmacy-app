/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.pharmacy.app.ui.data;

import java.util.Date;
import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class DrugInformationDetails {
    private Integer id;
    private String drugCode;
    private String drugName;
    private Double price;
    private Date expDate;
    private Date date;

    public DrugInformationDetails(Integer id, String drugCode, String drugName, Double price, Date expDate, Date date) {
        this.id = id;
        this.drugCode = drugCode;
        this.drugName = drugName;
        this.price = price;
        this.expDate = expDate;
        this.date = date;
    }
    private static final Logger LOG = Logger.getLogger(DrugInformationDetails.class.getName());

    public Integer getId() {
        return id;
    }

    public String getDrugCode() {
        return drugCode;
    }

    public String getDrugName() {
        return drugName;
    }

    public Double getPrice() {
        return price;
    }

    public Date getExpDate() {
        return expDate;
    }

    public Date getDate() {
        return date;
    }

    public static Logger getLOG() {
        return LOG;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setDrugCode(String drugCode) {
        this.drugCode = drugCode;
    }

    public void setDrugName(String drugName) {
        this.drugName = drugName;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public void setExpDate(Date expDate) {
        this.expDate = expDate;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
