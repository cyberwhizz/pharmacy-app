/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.pharmacy.app.impl;

import in.ac.gpckasaragod.pharmacy.app.service.DrugInformationService;
import in.ac.gpckasaragod.pharmacy.app.ui.data.DrugInformationDetails;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class DrugInformationServiceImpl extends ConnectionServiceImpl implements DrugInformationService{

    /**
     *
     * @param drugCode
     * @param drugName
     * @param price
     * @param expDate
     * @param date
     * @return
     */
    @Override
 public String saveDrugInformationDetailForm(String drugCode, String drugName, String price, Date expDate, Date date) {
         try{
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
             SimpleDateFormat dbDateFormat =  new SimpleDateFormat("yyyy-mm-dd");
             String formatedDate1 =dbDateFormat.format(expDate);
             String formatedDate2 =dbDateFormat.format(date);
            String query = "INSERT INTO DRUG_INFORMATION (DRUG_CODE,DRUG_NAME,PRICE,EXP_DATE,DATE) VALUES " +"('"+drugCode+ "','" +drugName+"','"+price+"','"+formatedDate1+"','"+formatedDate2+"')" ;
            System.out.println("Query:"+query);
            int status = statement.executeUpdate(query);
            if(status!=1){
                return "Save failed";
                }
            else{
                 return "Saved successfully";
            }
        } catch (SQLException ex) {
            Logger.getLogger(DrugInformationServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            return "Save Failed";
    }

    

    

 }
  
 @Override
    public List<DrugInformationDetails> getAllDrugInformations() {
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody//
      
        List<DrugInformationDetails> drugs = new ArrayList<>();
        try{
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "SELECT * FROM DRUG_INFORMATION";
            ResultSet resultSet = statement.executeQuery(query);
            
            while(resultSet.next()){
                int id = resultSet.getInt("ID");
                String drugCode = resultSet.getString("DRUG_CODE");
                String drugName = resultSet.getString("DRUG_NAME");
                Double price = resultSet.getDouble("PRICE");
                Date expDate = resultSet.getDate("EXP_DATE");
                Date date = resultSet.getDate("DATE");
                DrugInformationDetails drugInformationDetails = new DrugInformationDetails(id, drugCode, drugName, price, expDate, date);
                drugs.add(drugInformationDetails);
            }
        } catch (SQLException ex) {
         Logger.getLogger(DrugInformationServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
     }
     return drugs;
    }
    

     

    @Override
    public DrugInformationDetails readDrugInformationDetailsForm(Integer Id) {
         DrugInformationDetails  drugInformationDetails = null;
        try{
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "SELECT * FROM DRUG_INFORMATION WHERE ID="+Id;
            ResultSet resultSet = statement.executeQuery(query);
            while(resultSet.next()){
                int id = resultSet.getInt("ID");
                String drugCode = resultSet.getString("DRUG_CODE");
                String drugName = resultSet.getString("DRUG_NAME");
                Double price = resultSet.getDouble("PRICE");
                Date expDate = resultSet.getDate("EXP_DATE");
                Date date = resultSet.getDate("DATE");
                drugInformationDetails = new DrugInformationDetails(id, drugCode, drugName, price, expDate, date);
            }
            
            
        } catch (SQLException ex) {
         Logger.getLogger(DrugInformationServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
     }
       return drugInformationDetails;
    }

    @Override
    public String updateDrugInformationDetail(String drugCode, String drugName, String price, Date expDate, Date date) {
       try{
        Connection connection = getConnection();
        Statement statement = connection.createStatement();
        String query = "UPDATE DRUG_INFORMATIONS SET DRUG_CODE='"+drugCode+"',DRUG_NAME='"+drugName+"',PRICE='"+price+"',EXP_DATE='"+expDate+"',DATE='"+date+"";
        System.out.print(query);
        int update = statement.executeUpdate(query);
        if(update !=1)
            return "Update failed";
        else
            return "Updated successfully";
       
    }   catch (SQLException ex) {
            Logger.getLogger(DrugInformationServiceImpl.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public String deleteDrugInformationDetail(Integer id) {
        try{
            Connection connection =getConnection();
            String query = "DELETE FROM DRUG_INFORMATION WHERE ID=?";
            PreparedStatement statement = connection.prepareCall(query);
            statement.setInt(1, id);
            int delete = statement.executeUpdate();
            if (delete !=1)
                return "Delete Failed";
            else
                return "Deleted Successfully";
            
            
        } catch (Exception ex) {
            ex.printStackTrace();
            return "Deleted Failed";
    }
    }
    }

   
    



      
    




                
                