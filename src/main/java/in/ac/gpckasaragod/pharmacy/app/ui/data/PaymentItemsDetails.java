/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.pharmacy.app.ui.data;

import java.util.logging.Logger;

/**
 *
 * @author alanster
 */
public class PaymentItemsDetails {
    private Integer id;
    private Integer drugId;
    private Integer paymentId;
    private Integer quantity;
    private Double unitPrice;
    private Double subTotal;
    
    public PaymentItemsDetails(Integer id, Integer drugId, Integer paymentId, Integer quantity, Double unitPrice, Double subTotal) {
        this.id = id;
        this.drugId = drugId;
        this.paymentId = paymentId;
        this.quantity = quantity;
        this.unitPrice = unitPrice;
        this.subTotal = subTotal;
        
    }
    private static final Logger LOG = Logger.getLogger(PaymentItemsDetails.class.getName());

    public PaymentItemsDetails(int id, String drugId, String paymentId, String quantity, String unitPrice, String subtotal) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDrugId() {
        return drugId;
    }

    public void setDrugId(Integer drugId) {
        this.drugId = drugId;
    }

    public Integer getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(Integer paymentId) {
        this.paymentId = paymentId;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(Double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public Double getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(Double subTotal) {
        this.subTotal = subTotal;
    }
    
}
