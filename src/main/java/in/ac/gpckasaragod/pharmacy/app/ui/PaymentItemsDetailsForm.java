/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package in.ac.gpckasaragod.pharmacy.app.ui;

import in.ac.gpckasaragod.pharmacy.app.service.PaymentItemsService;
import in.ac.gpckasaragod.pharmacy.app.ui.data.PaymentItemsDetails;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author student
 */
public class PaymentItemsDetailsForm extends javax.swing.JFrame {
 
    public PaymentItemsService paymentItemsService= new paymentItemsServiceImpl();
    /**
     * Creates new form PaymentItemsDetails
     */
    public PaymentItemsDetailsForm() {
        initComponents();
        configureTable();
        loadTableValues();
    }
    public PaymentItemsDetailsForm(Integer drugId, Integer paymentId, Integer quantity, Double unitPrice, Double subTotal) {
    
}
      private void configureTable(){
        DefaultTableModel model = new DefaultTableModel();
        model.addColumn("Id");
        model.addColumn("Drug_id");
        model.addColumn("Payment_id");
        model.addColumn("Quantity");
        model.addColumn("Unit_price");
        model.addColumn("Sub_total");
        tblPayment.setModel(model);
    }
       private void loadTableValues(){
        List<PaymentItemsDetails> payments = paymentItemsService.getAllPaymentItems();
        DefaultTableModel model = (DefaultTableModel) tblPayment.getModel();
        model.setRowCount(0);
        for(PaymentItemsDetails paymentDetails :payments){
            model.addRow(new Object[]{paymentDetails.getId(),paymentDetails.getDrugId(),paymentDetails.getPaymentId(),paymentDetails.getQuantity(),paymentDetails.getUnitPrice(),paymentDetails.getSubTotal()});
        }
         
        
   
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tblPayment = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        btnAdd = new javax.swing.JButton();
        btnEdit = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        tblPayment.setFont(new java.awt.Font("Liberation Sans", 0, 18)); // NOI18N
        tblPayment.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "ID", "DRUG ID", "PAYMENT ID", "QUANTITY", "UNIT PRICE", "SUBTOTAL"
            }
        ));
        jScrollPane1.setViewportView(tblPayment);

        jLabel1.setFont(new java.awt.Font("Liberation Serif", 1, 24)); // NOI18N
        jLabel1.setText("PAYMENT ITEMS");

        btnAdd.setText("ADD");
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });

        btnEdit.setText("EDIT");
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });

        btnDelete.setText("DELETE");
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(58, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 738, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnAdd)
                        .addGap(246, 246, 246)
                        .addComponent(btnEdit)
                        .addGap(260, 260, 260)
                        .addComponent(btnDelete)))
                .addGap(80, 80, 80))
            .addGroup(layout.createSequentialGroup()
                .addGap(318, 318, 318)
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 31, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAdd)
                    .addComponent(btnEdit)
                    .addComponent(btnDelete))
                .addGap(70, 70, 70))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
       int rowIndex = tblPayment.convertRowIndexToModel(tblPayment.getSelectedRow());
           Integer id = (Integer) tblPayment.getValueAt(rowIndex, 0);
           int result = JOptionPane.showConfirmDialog(PaymentItemsDetailsForm.this, "Are you sure to delete");
           if(result == JOptionPane.YES_OPTION){
               String message = paymentItemsService.deletePaymentItemsDetails(id);
               JOptionPane.showMessageDialog(PaymentItemsDetailsForm.this, message);
               loadTableValues();
               
            }
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        JFrame window = new JFrame();
        window.setVisible(true);
        PaymentItemsDetailForm paymentItemsDetailForm = new PaymentItemsDetailForm();
       window.add(paymentItemsDetailForm);
       window.pack();
        window.setLocationRelativeTo(null);
        window.addWindowListener(new WindowAdapter(){
            @Override
            public void windowClosed(WindowEvent e){
                loadTableValues();
                System.out.print("Winodw Closed");
            }
            @Override
            public void windowClosing(WindowEvent e){
                super.windowClosing(e);
                System.out.print("Window Closing");
                loadTableValues();
                
           }

        });
               
    }//GEN-LAST:event_btnAddActionPerformed

    private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
        int rowIndex = tblPayment.convertRowIndexToModel(tblPayment.getSelectedRow());
        Integer id = (Integer) tblPayment.getValueAt(rowIndex, 0);
        PaymentItemsDetails paymentItemsDetails = paymentItemsService.readPaymentItemsDetailsForm(id);
        JFrame window = new JFrame();
        window.setVisible(true);
        PaymentItemsDetailForm paymentItemsDetailForm = new PaymentItemsDetailForm(paymentItemsDetails);
        window.add(paymentItemsDetailForm);
        window.setSize(600, 600);
        window.pack();
        window.setLocationRelativeTo(null);
        window.addWindowListener(new WindowAdapter(){
            @Override
            public void windowClosed(WindowEvent e){
                loadTableValues();
                System.out.print("Window Closed");
            }
            @Override
            public void windowClosing(WindowEvent e){
                super.windowClosing(e);
                System.out.print("Window Closing");
                loadTableValues();
            }

             private void loadTableValues() {
                 throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
             }
        });
    }//GEN-LAST:event_btnEditActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(PaymentItemsDetailsForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(PaymentItemsDetailsForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(PaymentItemsDetailsForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(PaymentItemsDetailsForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new PaymentItemsDetailsForm().setVisible(true);
            }
        });
    } 

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnEdit;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblPayment;
    // End of variables declaration//GEN-END:variables

    private void initComponents() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
}