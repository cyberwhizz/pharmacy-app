/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.pharmacy.app.service;

import in.ac.gpckasaragod.pharmacy.app.ui.data.PaymentItemsDetails;
import java.util.List;

/**
 *
 * @author Exam
 */
public interface PaymentItemsService {
    public String saveDrugInformationDetailForm(String drugId, String paymentId, String quantity, String unitPrice, String subtotal);
     public  PaymentItemsDetails readPaymentItemsDetailsForm(Integer Id);
      public List<PaymentItemsDetails> getAllPaymentItems();
      public String updatePaymentItemsDetail(String drugId, String paymentId, String quantity, String unitPrice, String subtotal);
       public String deletePaymentItemsDetail(Integer id);

    public String deletePaymentItemsDetails(Integer id);
}
